package ru.t1.volkova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.model.Task;

import java.util.List;

@SuppressWarnings("UnusedReturnValue")
public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @NotNull String description
    );

    @NotNull
    Task updateByIndex(
            @NotNull String userId,
            @Nullable Integer index,
            @Nullable String name,
            @NotNull String description
    );

    @NotNull
    Task changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @NotNull Status status
    );

    @NotNull
    Task changeTaskStatusByIndex(
            @NotNull String userId,
            @Nullable Integer index,
            @NotNull Status status
    );

    @Nullable
    List<Task> findAllByProjectId(
            @NotNull String userId,
            @Nullable String projectId
    );

}
