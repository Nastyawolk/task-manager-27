package ru.t1.volkova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Start task status by id.";

    @NotNull
    private static final String NAME = "task-start-by-id";

    @Override
    public void execute() {
        System.out.println("[START TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        taskService().changeTaskStatusById(userId, id, Status.IN_PROGRESS);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
